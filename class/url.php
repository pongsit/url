<?php

namespace pongsit\url;
	
class url{
	
	public function __construct() 
    {
    }
    
	function get_current($type=''){
		$url = 'http'.(isset($_SERVER['HTTPS'])?'s':'').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		if($type=='array'){
			$urls = parse_url($url);
			$pathinfos = pathinfo($urls['path']);
			return array_merge($urls,$pathinfos);
			/*
			Array
			(
			    [scheme] => http
			    [host] => localhost
			    [path] => /mart/accountant.php
			    [dirname] => /mart
			    [basename] => accountant.php
			    [extension] => php
			    [filename] => accountant
			)
			*/
		}else{
			return $url;	
		}
	}
	
	function get_info(){
		$url = 'http'.(isset($_SERVER['HTTPS'])?'s':'').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$urls = parse_url($url);
		$pathinfos = pathinfo($urls['path']);
		return array_merge($urls,$pathinfos);
		/*
		Array
		(
			[scheme] => http
			[host] => localhost
			[path] => /mart/accountant.php
			[dirname] => /mart
			[basename] => accountant.php
			[extension] => php
			[filename] => accountant
		)
		*/
	}
	
	function get_current_page(){
		$pathInfos = pathinfo($_SERVER['REQUEST_URI']);
		if(!empty($pathInfos['extension'])){
			$basenames = explode('?',$pathInfos['basename']);
			$output = $basenames[0];
		}else{
			$output = 'index.php';
		};
		return $output;
	}
	
	function get_current_basename(){
		return basename($_SERVER['PHP_SELF']);
	}
	
	function check_ie(){
		$u_agent = $_SERVER['HTTP_USER_AGENT'];
		if(preg_match('/MSIE/i',$u_agent)){
			return true;
		}
		return false;
	}
}